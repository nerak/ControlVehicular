<!DOCTYPE html>

<?php session_start(); if (@!$_SESSION[ 'user']) { header( "Location:index.php"); }elseif ($_SESSION[ 'rol']==1) { header( "Location:admin.php"); } ?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Proyecto Academias</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Joseph Godoy">

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="bootstrap/js/jquery-1.8.3.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>


    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
</head>

<body data-offset="40" background="images/fondotot.jpg" style="background-attachment: fixed">
    <div class="container">
        <header class="header">
            <div class="row">
                <?php include( "include/cabecera.php"); ?>
            </div>
        </header>

        <!-- Navbar
    ================================================== -->
        <?php include( "include/menu.php"); ?>
        <!-- ======================================================================================================================== -->






    </div>
    <center>
        <h3>Imágenes de los vehículos oficiales</h3>
    </center>
    <div class="row">

        <div class="span4">
            <div class="thumbnail">
                <h3 style="text-align:center">Combi tinto</h3>	
                <img src="images/veh_1.png" alt="#" />
                <div class="caption">

                    <a class="pull-right" href="al.php">Ver detalles</a>
                    <br/>
                </div>
            </div>
        </div>

        <div class="span4">
            <div class="thumbnail">
                <h3 style="text-align:center">Camioneta negra</h3>	
                <img src="images/veh_2.png" />
                <div class="caption">

                    <a class="pull-right" href="ta.php">Ver detalles</a>
                    <br/>
                </div>
            </div>
        </div>

        <div class="span4">
            <div class="thumbnail">
                <h3 style="text-align:center">Camioneta blanca</h3>	
                <img src="images/veh_3.png" />
                <div class="caption">

                    <a class="pull-right" href="cb.php">Ver detalles</a>
                    <br/>
                </div>
            </div>
        </div>

         <div class="span4">
            <div class="thumbnail">
                <h3 style="text-align:center">Combi blanca</h3>	
                <img src="images/veh_4.png" />
                <div class="caption">

                    <a class="pull-right" href="cb.php">Ver detalles</a>
                    <br/>
                </div>
            </div>
        </div>

         <div class="span4">
            <div class="thumbnail">
                <h3 style="text-align:center">Camioneta blanca</h3>	
                <img src="images/veh_3.png" />
                <div class="caption">

                    <a class="pull-right" href="cb.php">Ver detalles</a>
                    <br/>
                </div>
            </div>
        </div>


    </div>
    <hr/>
    <div class="row">
        <div class="span6">

        </div>
        <div class="span6">

        </div>


    </div>
    <!-- Footer
      ================================================== -->
    <hr class="soften" />
    <footer class="footer">

        <hr class="soften" />
        <p>&copy; Copyright SMART Innovations
            <br/>
            <br/>
        </p>
    </footer>
    </div>
    <!-- /container -->

    </style>
</body>

</html>