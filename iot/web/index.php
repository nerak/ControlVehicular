<!--A Design by W3layouts
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Control de Vehículos Oficiales</title>
	<!-- Meta tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Angling Booking Form Responsive Widget, Audio and Video players, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design"
	/>
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!-- Meta tags -->
	<!-- Calendar -->
	<link rel="stylesheet" href="css/jquery-ui.css" />
	<!-- //Calendar -->
	<!--stylesheets-->
	<link href="css/style.css" rel='stylesheet' type='text/css' media="all">
	<!--//style sheet end here-->
	<link href="//fonts.googleapis.com/css?family=Cuprum:400,700" rel="stylesheet">
</head>

<body>
	<h1 class="header-w3ls">
		Control de Vehículos Oficiales</h1>
		<center><img class="mdfk" src="images/logo_oficial.png" width="200px" height="200px"></center>
	<div class="appointment-w3">
		<form action="validar.php" method="post">
			<div class="main">
				<div class="form-left-w3l">
					<input type="text" class="top-up" name="mail" placeholder="Usuario" required="">
				</div>
				<div class="form-right-w3ls">

					<input type="password" class="top-up" name="pass" placeholder="Contraseña" required="">
				</div>

			</div>

			<div class="btnn">
				<input type="submit" value="Entrar">
			</div><br><br>
			<div class="form-left-w3l">

					<a href=""><p class="recover">¿ Olvidaste Contraseña ?</p></a>
				</div>
		</form>
	</div>
	<div class="copy">
		<p>&copy;2017 Todos los Derechos Reservados | Diseñado por <a href="https://smart.innovations.lak-solutions.com/" target="_blank">SMART Innovations</a></p>
	</div>
	<!-- js -->
	<script type='text/javascript' src='js/jquery-2.2.3.min.js'></script>
	<!-- //js -->
	<!-- Calendar -->
	<script src="js/jquery-ui.js"></script>
	<script>
		$(function () {
			$("#datepicker,#datepicker1,#datepicker2,#datepicker3").datepicker();
		});
	</script>
	<!-- //Calendar -->
</body>

</html>