<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/style_tab.css">
</head>
<body>

<p class="title">Control de vehículos oficiales</p>

<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">Control de Solicitudes</button>
  <button class="tablinks" onclick="openCity(event, 'Paris')">Reportes Mensuales</button>
  <button class="tablinks" onclick="openCity(event, 'Tokyo')">A new one</button>
</div>

<div id="London" class="tabcontent">
  <span onclick="this.parentElement.style.display='none'" class="topright">x</span>
  <h3>Ventana 1</h3>
  <p>Ventana 1 contenido.</p>
</div>

<div id="Paris" class="tabcontent">
  <span onclick="this.parentElement.style.display='none'" class="topright">x</span>
  <h3>Ventana 2</h3>
  <p>Ventana 2 contenido.</p> 
</div>

<div id="Tokyo" class="tabcontent">
  <span onclick="this.parentElement.style.display='none'" class="topright">x</span>
  <h3>Ventana 3</h3>
  <p>Ventana 3 contenido.</p>
</div>

<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
     
</body>
</html> 